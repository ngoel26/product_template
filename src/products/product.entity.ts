import {BaseEntity, Entity, PrimaryGeneratedColumn,Column} from 'typeorm'

@Entity()
export class ProductEntity extends BaseEntity{
@PrimaryGeneratedColumn() 
id:number;

@Column() 
pname: string;
@Column()
description: string;
@Column()
quantity: number;
@Column()
MDate: Date;
@Column()
CDate: Date;
}