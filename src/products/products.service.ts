import { Injectable, HttpStatus} from '@nestjs/common';
//import { Product } from './product.model';
import {InjectRepository} from '@nestjs/typeorm'
import {Repository} from 'typeorm'
import {ProductEntity} from './product.entity'
//import {v4 as uuidv4} from 'uuid'
@Injectable()
export class ProductsService {
    constructor( @InjectRepository(ProductEntity)
    private productRepository: Repository<ProductEntity>){
       
    }



async getAllProducts(){
    return await this.productRepository.find();
}
   
async getProductsById(id: number){
    return await this.productRepository.findOne({where:{id:id}});
}

async deleteProduct(id: number)
{
    await this.productRepository.delete({id});
    return{deleted:true}
}

async createProduct(pname:string, description:string, quantity:number, MDate:Date, CDate:Date)
{
    const product = await this.productRepository.create({pname,description,quantity,MDate,CDate})
    await this .productRepository.save({pname,description,quantity,MDate,CDate})
    return product;
}

 async updateProduct(id:number, pname:string)
 {
     await this.productRepository.update({id}, {pname});
     return await this.productRepository.findOne({id});
 }
}
