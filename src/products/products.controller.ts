import { Body, Controller, Get, Post, HttpStatus, Param, Delete, Patch } from '@nestjs/common';
import { get } from 'https';
import {ProductsService} from './products.service'
@Controller('products')
export class ProductsController {
    constructor(private productService: ProductsService){}

    @Get()
    async getAllProducts(){
        return {
            statusCode:HttpStatus.OK,
            data: await this.productService.getAllProducts()
        }
        
    }
@Get(':id')
async getProductsById(@Param('id') id:number)
{
    return {
        statusCode:HttpStatus.OK,
        data: await this.productService.getProductsById(id)
    }
}
    @Post()
    async createProduct(
        @Body('name') name:string,
        @Body('description') description:string,
        @Body('quantity') quantity:number,
        @Body('mdate') mdate:Date, 
        @Body('cdate') cdate:Date){
            return {

                statusCode:HttpStatus.OK,

                message: 'Product added successfully',
                data: await this.productService.createProduct(name, description, quantity, mdate, cdate),

            }
            
        }

    @Delete(':id') 
    
    async deleteProduct(@Param('id') id: number)
    {
        await this.productService.deleteProduct(id);
        return{
            statusCode:HttpStatus.OK,
            message: 'Product deleted successfully',
        }
    }

    @Patch(':id')
    async updateProduct(@Param('id') id: number, @Body('name') name:string )
    {
        return {

            statusCode:HttpStatus.OK,

            message: 'Product updated successfully',
            data: await this.productService.updateProduct(id,name),

        }

    }
    
       
}
