export interface Product {
    id: string;
    name: string;
    description: string;
    quantity:number;
    mdate:Date;
    cdate:Date;
}